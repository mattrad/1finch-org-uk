<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/w3j6kTDTlYF9phBECA+lZHkMOsMwDZMKPwlr3pO5gYosyt30WhY4SV1Q8Rp3RLIaQ4zI8NQt3ZOWawTu8x6ag==');
define('SECURE_AUTH_KEY',  'a7zgHwI7TZtbo+w1WJ8Gs0k5f6aBfHFglIye7IDI/yEFJKXolJyyDjoBraN5AL3U2pqTgOSr/nHo+dsZrmK0SQ==');
define('LOGGED_IN_KEY',    'WTxzPnUyfVz/cSNlm3Sj7uEubZUk/2WC7G0bsX9trwkteZ0YUoGngKHyLF/EJQzNnTFPncBpgNhFfWylvCFvfw==');
define('NONCE_KEY',        'qHIaNR0rO8CKyfuh83OktdgX2GYMm3Ptvtvrpk2CjeCeumn5Cjjcmr+Cf9aIrHU7KExHPD3gz8NJ2qDKkS4sFg==');
define('AUTH_SALT',        'ZrDJfQ+CicnbRT7PKRNi43kfLxWilDKz7aiwSc6iI0IhbSwMEftCMs6PN/EhJsDCkZdR055/7C/3HhxlnFlZ4Q==');
define('SECURE_AUTH_SALT', '/KnnB2Y+uh9ENnprP7l12mE9TxdwUX2qCjbWnV/MVP4lqgkTHRU11+OB/QQyPJcJVH3vcOjLTiHlzm0Y+U+19g==');
define('LOGGED_IN_SALT',   '2FEMB6cTOkvQv1/P9iiQ0KVzMCu7v2zivx8iaukeR8xaUtVvmi/Lik34BeTbJ3+BYdTLTz2pT6tZ0TGHGo/NVA==');
define('NONCE_SALT',       'h9OhM90ArRLn2CH8qAzLQx8PlHgCjVEcNlUG1JVimb0JRZ7cohf/f4dleR2tUXbazTqhgS0bLMQmW3fByXs+VQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define( 'WP_CONTENT_DIR', dirname(__FILE__) . '/wp-content' );
define( 'WP_CONTENT_URL', 'http://1finch.local/wp-content' );

/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
