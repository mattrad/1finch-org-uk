<?php
/*
 Plugin Name: Gravity Forms Editor Access
 Plugin URI: https://mattrad.uk
 Description: Allow Editors view access to Gravity Forms entries
 Version: 1.0.0
 Author: Matt Radford
 Author URI: https://mattrad.uk
 */
/**
 * Add all Gravity Forms capabilities to Editor role.
 * Runs during plugin activation.
 * 
 * @access public
 * @return void
 */
function activate_gf_editor_access() {
  
  $role = get_role( 'editor' );
  $role->add_cap( 'gravityforms_view_entries' );
}
// Register our activation hook
register_activation_hook( __FILE__, 'activate_gf_editor_access' );

/**
 * Remove Gravity Forms capabilities from Editor role.
 * Runs during plugin deactivation.
 * 
 * @access public
 * @return void
 */
function deactivate_gf_editor_access() {
 
 $role = get_role( 'editor' );
 $role->remove_cap( 'gravityforms_view_entries' );
}
// Register our de-activation hook
register_deactivation_hook( __FILE__, 'deactivate_gf_editor_access' );