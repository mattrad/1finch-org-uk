<?php
/**
 * Plugin Name: Event Organiser Mods
 * Plugin URI: https://mattrad.uk
 * Version: 1.0.0
 * Description: Disable event single posts. Remove links from calendar.
 * Author: Matt Radford
 * Author URI: https://mattrad.uk
*/

/**
 * Remove link from calendar
*/
function mattrad_no_calendar_link($link, $event_id, $occurrence_id) {
	return false;
}
add_filter('eventorganiser_calendar_event_link','mattrad_no_calendar_link',10,3);

/**
 * Redirect event single
*/
function mattrad_redirect_event_single() {
	if (!is_singular('event')) {
		return;
	}
    wp_redirect(get_home_url(),302);
    exit;
}
add_action('template_redirect', 'mattrad_redirect_event_single');
