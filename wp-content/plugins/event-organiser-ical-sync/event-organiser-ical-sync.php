<?php
/**
 * Plugin Name: Event Organiser ICAL Sync
 * Plugin URI: http://www.wp-event-organiser.com
 * Version: 2.3.0
 * Description: Automatically import ICAL feeds from other sites / Google
 * Author: Stephen Harris
 * Author URI: http://www.stephenharris.info
*/
/**  Copyright 2013 Stephen Harris (contact@stephenharris.info)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

//Initiates the plug-in.
add_action( 'plugins_loaded', array( 'EO_Sync_Ical', 'init' ) );

if ( defined( 'WP_CLI' ) && WP_CLI ) {
	require_once( dirname( __FILE__ ) . '/wp-cli/class-eo-ical-wp-cli.php' );
}

/**
 * @ignore
 * @author stephen
 */
class EO_Sync_Ical {

	/**
	 * Instance of the class
	 * @static
	 * @access protected
	 * @var object
	 */
	protected static $instance;

	static $version = '2.3.0';

	/**
	 * Instantiates the class
	 * @return object $instance
	 */
	public static function init() {
		is_null( self :: $instance ) && self :: $instance = new self;
		return self :: $instance;
	}

	/**
	 * Constructor.
	 * @return \Post_Type_Archive_Links
	 */
	public function __construct() {

		if ( defined( 'EVENT_ORGANISER_VER' ) ) {
			//Load ical functions.
			require_once( plugin_dir_path( __FILE__ ) . 'ical-functions.php' );

			if ( version_compare( '2.7', EVENT_ORGANISER_VER ) <= 0 ) {
				require_once( plugin_dir_path( __FILE__ ) . 'addon.php' );
			}

			//Load hooks.
			$this->hooks();
		}

	}

	function hooks() {

		add_action( 'after_setup_theme', array( $this, 'setup_constants' ) );

		add_action( 'init', array( $this, 'register_feed_posttype' ) );

		add_action( 'eventorganiser_event_settings_imexport', array( $this, 'display_feeds' ), 5 );

		add_action( 'wp_ajax_add-eo-feed', array( $this, 'ajax_add_feed' ) );
		add_action( 'wp_ajax_delete-eo-feed', array( $this, 'ajax_delete_feed' ) );
		add_action( 'wp_ajax_fetch-eo-feed', array( $this, 'ajax_fetch_feed' ) );

		add_action( 'load-settings_page_event-settings', array( $this, 'update_feed_settings' ) );

	}

	function register_feed_posttype() {

		$labels = array(
			'name'                => _x( 'Feeds', 'Post Type General Name', 'eventorganiserical' ),
			'singular_name'       => _x( 'Feed', 'Post Type Singular Name', 'eventorganiserical' ),
			'view_item'           => __( 'View feeds', 'eventorganiserical' ),
			'add_new_item'        => __( 'Add New Feed', 'eventorganiserical' ),
			'add_new'             => __( 'Add Feed', 'eventorganiserical' ),
			'edit_item'           => __( 'Edit Feed', 'eventorganiserical' ),
			'update_item'         => __( 'Update Feed', 'eventorganiserical' ),
		);

		$args = array(
			'description'         => __( 'ICAL Feed', 'eventorganiserical' ),
			'labels'              => $labels,
			'supports'            => array( 'title' ),
			'hierarchical'        => false,
			'public'              => false,
			'show_ui'             => false,
			'show_in_menu'        => false,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => false,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'rewrite'             => false,
			'capability_type'     => 'post',
		);

		register_post_type( 'eo_icalfeed', $args );
	}

	function setup_constants() {
		define( 'EVENT_ORGANISER_ICAL_SYNC_URL', plugin_dir_url( __FILE__ ) );
	}

	function display_feeds() {

		$ext = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
		wp_enqueue_script( 'eo-sync-ical', EVENT_ORGANISER_ICAL_SYNC_URL . "js/eo-sync-ical{$ext}.js", array( 'jquery', 'wp-lists', 'common' ), self::$version );
		wp_enqueue_style( 'eo-sync-ical', EVENT_ORGANISER_ICAL_SYNC_URL . "css/eo-sync-ical{$ext}.css", array(), self::$version );
		?>	

		<h3><?php esc_html_e( 'iCal Feeds', 'eventorganiserical' ); ?></h3>
	
		<?php $feeds = eo_get_feeds(); ?>		
		<div id="col-container">
			<div id="col-right">
					
			<table class="wp-list-table widefat fixed tags" id="feed-list" cellspacing="0" data-wp-lists="list:eo-feed">
				<thead>
					<tr>
						<th scope="col" id="name" class="" style="">
							<?php esc_html_e( 'Name', 'eventorganiserical' ); ?>
						</th>
						
						<th scope="col" id="url" class="" style="">
							<?php esc_html_e( 'Source', 'eventorganiserical' ); ?>
						</th>
						
						<th scope="col" id="last-updated" class="" style="">
							<?php esc_html_e( 'Last Fetched', 'eventorganiserical' ); ?>
						</th>
						<th scope="col" id="feed-events" class="" style="">
							<?php esc_html_e( 'Events', 'eventorganiser' ); ?>
						</th>
					</tr>
				</thead>
				<tbody>	
					<?php
					if ( $feeds ) :
						foreach ( $feeds as $feed ) :
							$this->display_feed_row( $feed );
						endforeach;
					endif;
					?>
					<tr id="eo-feed-no-feeds" <?php if ( $feeds ) { echo 'style="display:none"'; };?>>
						<td colspan="4"> <?php esc_html_e( 'No feeds', 'eventorganiserical' ); ?></td>
					</tr>
				</tbody>
				
			</table>
			<div class="form-wrap">	
				<form id="eo-feed-settings" method="post" class="validate">
					<input type="hidden" name="action" value="eventorganier-update-feed-settings" />

					<div class="form-required">
						<label for="sync-schedule"> 
						<?php
						esc_html_e( 'Sync schedule:', 'eventorganiserical' ) . ' ';
						eventorganiser_select_field( array(
							'id'       => 'sync-schedule',
							'options'  => eo_get_feed_sync_schedules(),
							'selected' => get_option( 'eventorganiser_feed_schedule' ),
							'name'     => 'eventorganiser_feed_schedule',
						));

						wp_nonce_field( 'eventorganier-update-feed-settings' );

						submit_button(
							__( 'Update feed settings', 'eventorganiserical' ), 'secondary', 'submit', false,
							array(
								'id' => 'feed-settings-submit',
							)
						);
						?>
						</label>
						<?php
						if ( $timestamp = wp_next_scheduled( 'eventorganiser_ical_feed_sync' ) ) {
							$timestamp = (int) $timestamp;
							$date_obj = new DateTime( '@'.$timestamp );
							$date_obj->setTimezone( eo_get_blog_timezone() );
							printf(
								esc_html__( 'Next feed sync is scheduled for %s', 'eventorganiserical' ),
								eo_format_datetime( $date_obj, get_option( 'date_format' ) . ' ' .  get_option( 'time_format' ) )
							);
						}
						?>
					</div>				
				</form>
			</div>
									
			</div>
			
			<div id="col-left">
				<div class="col-wrap">
				
					<div class="form-wrap">
						<h4> <?php esc_html_e( 'Add New Feed', 'eventorganiserical' ); ?></h4>
						
						<form id="add-eo-feed" method="post" class="validate">

							<div class="form-field form-required">
								<label for="feed-name"> <?php esc_html_e( 'Name', 'eventorganiserical' ); ?> </label>
								<input name="feed-name" id="feed-name" type="text" value="" size="40" aria-required="true">
							</div>

							<div class="form-field form-required">
								<label for="feed-source"><?php esc_html_e( 'Source', 'eventorganiserical' ); ?></label>
								<input name="feed-source" id="feed-source" type="text" value="" size="40" aria-required="true">
							</div>

							<p class="hide-if-no-js">
								<a href="#" class="eo-advanced-feed-options-toggle eo-show-advanced-option">
									<?php esc_html_e( 'Show advanced options', 'eventorganiserical' ); ?>
								</a>
								<a href="#" class="eo-advanced-feed-options-toggle eo-hide-advanced-option hide-if-js">
									<?php esc_html_e( 'Hide advanced options', 'eventorganiserical' ); ?>
								</a>
							</p>

							<div id="eo-advanced-feed-options-wrap" class="hide-if-js">
								<div class="form-field">
									<label for="feed-organiser"><?php esc_html_e( 'Assign events to', 'eventorganiserical' ); ?></label>
									<?php
									wp_dropdown_users( array(
										'id'       => 'feed-organiser',
										'name'     => 'feed-organiser',
										'selected' => get_current_user_id(),
									));
									?>
								</div>

								<div class="form-field">
									<label for="feed-category"><?php esc_html_e( 'Assign events to category', 'eventorganiserical' ); ?></label>
									<?php
									wp_dropdown_categories( array(
										'show_option_none' => __( 'Use category specified in feed', 'eventorganiserical' ),
										'orderby'      => 'name',
										'hide_empty'   => 0,
										'hierarchical' => 1,
										'name'         => 'feed-category',
										'id'           => 'feed-category',
										'taxonomy'     => 'event-category',
									));
									?>
								</div>

								<div class="form-field">
									<label for="feed-venue"><?php esc_html_e( 'Assign events to venue', 'eventorganiserical' ); ?></label>
									<?php
									wp_dropdown_categories( array(
										'show_option_none' => __( 'Use venue specified in feed', 'eventorganiserical' ),
										'orderby'      => 'name',
										'hide_empty'   => 0,
										'hierarchical' => 1,
										'name'         => 'feed-venue',
										'id'           => 'feed-venue',
										'taxonomy'     => 'event-venue',
									));
									?>
								</div>

								<div class="form-field">
									<label for="feed-status"><?php esc_html_e( 'Event status', 'eventorganiserical' ); ?></label>
									<?php
									eventorganiser_select_field(array(
										'name'    => 'feed-status',
										'id'      => 'feed-status',
										'options' => array_merge( array( '0' => __( 'Use status specified in feed', 'eventorganiserical' ) ), get_post_statuses() ),
									));
									?>
								</div>
							</div>
							<p class="submit">
								<?php
								$nonce = wp_create_nonce( 'add-eo-feed-0' );
								submit_button(
									__( 'Add new feed', 'eventorganiserical' ),
									'primary',
									'submit',
									false,
									array(
										'data-wp-lists' => 'add:feed-list:add-eo-feed::_ajax_nonce=' . $nonce,
										'id'        => 'add-eo-feed-submit',
									)
								);
								?>
								<span class="spinner" style="float: none;display: inline-block;visibility: hidden;"></span>
							</p>
						</form>
					</div>
				</div>
			</div>

		</div>
	<?php
	}

	function display_feed_row( $feed ) {
		$del_nonce   = wp_create_nonce( 'delete-eo-feed-' . $feed->ID );
		$upd_nonce   = wp_create_nonce( 'add-eo-feed-' . $feed->ID );
		$fetch_nonce = wp_create_nonce( 'fetch-eo-feed-' . $feed->ID );
		$source      = get_post_meta( $feed->ID, '_eventorganiser_feed_source', true );
		$error       = maybe_unserialize( get_post_meta( $feed->ID, '_eventorganiser_feed_log', true ) );
		$warnings    = get_post_meta( $feed->ID, '_eventorganiser_feed_warnings' );

		$user_id  = get_post_meta( $feed->ID, '_eventorganiser_feed_organiser', true );
		$status   = get_post_meta( $feed->ID, '_eventorganiser_feed_status', true );
		$category = get_post_meta( $feed->ID, '_eventorganiser_feed_category', true );
		$venue    = get_post_meta( $feed->ID, '_eventorganiser_feed_venue', true );

		$class = 'eo-ical-feed-unrun';

		if ( $error ) {
			$class = 'eo-ical-feed-with-errors';
		} elseif ( $warnings ) {
			$class = 'eo-ical-feed-with-warnings';
		} elseif ( eo_get_feed_last_fetched( $feed->ID ) ) {
			$class = 'eo-ical-feed-success';
		}
		?>
		<tbody id="eo-feed-<?php echo $feed->ID;?>" class="eo-ical-feed <?php echo $class; ?>">
				
		<tr class="feed-row">		
			<td class="name">
				<strong class="eo-ical-feed-name" tabindex=0> <?php echo esc_html( $feed->post_title ); ?></strong>
								
				<div class="row-actions">
					<span class="edit"><a href="#">Edit Feed </a> |</span>
					<span class="delete">
						<a class="delete-feed" data-wp-lists="delete:feed-list:eo-feed-<?php echo $feed->ID;?>::_ajax_nonce=<?php echo $del_nonce; ?>" href="#">
							<?php esc_html_e( 'Delete', 'eventorganiserical' ); ?>
						</a> 
					| </span>
					<span class="fetch"> 
						<a class="fetch-feed" data-wp-lists="dim:feed-list:eo-feed-<?php echo $feed->ID;?>:dimclass:::action=fetch-eo-feed&_ajax_nonce=<?php echo $fetch_nonce; ?>" href="#">
							<?php esc_html_e( 'Fetch now', 'eventorganiserical' ); ?>
						</a>
					</span>
				</div>

			</td>
							
			<td class="source">
				<?php echo esc_html( $source ); ?> 
			</td>
						
			<td class="last-updated">
				<span class="eo-ical-feed-last-updated-date">
				<?php
				$time = eo_get_feed_last_fetched( $feed->ID );
				if ( $time ) {
					$time_diff = time() - $time;
					if ( $time_diff > 0 && $time_diff < DAY_IN_SECONDS ) {
						echo esc_html( sprintf( __( '%s ago' ), human_time_diff( $time ) ) );
					} else {
						echo esc_html( date_i18n( get_option( 'date_format' )  . '\<\b\r\/\> ' . get_option( 'time_format' ), $time ) );
					}
				} else {
					printf( '<span aria-hidden="true">—</span><span class="screen-reader-text">%s</span>', esc_html__( 'Not synced', 'eventorganiserical' ) );
				}
				?>
				</span>
				<span class="spinner" style="float: none;display: inline-block;visibility: hidden;"></span>
			</td>
			
			<td class="feed-events">
				<?php
				$events = get_post_meta( $feed->ID, '_eventorganiser_feed_events_parsed', true );
				if ( '' !== $events ) {
					echo (int) $events;
				} else {
					printf( '<span aria-hidden="true">—</span><span class="screen-reader-text">%s</span>', esc_html__( 'Not synced', 'eventorganiserical' ) );
				}
				?>
			</td>
								
			<td class="edit-column" style="display:none" colspan="4">

				<fieldset>
					<div class="inline-edit-col">
						<h4><?php esc_html_e( 'Quick Edit', 'eventorganiserical' ); ?></h4>

						<label>
							<span class="title"><?php esc_html_e( 'Name', 'eventorganiserical' ); ?></span>
							<span class="input-text-wrap">
								<input type="text" name="feed-name" class="ptitle" value="<?php echo esc_attr( $feed->post_title ); ?>">
							</span>
						</label>
						<label>
							<span class="title"><?php esc_html_e( 'Slug', 'eventorganiserical' ); ?></span>
							<span class="input-text-wrap">
								<input type="text" name="feed-source" class="ptitle" value="<?php echo esc_attr( $source ); ?>">
							</span>
						</label>
						<label>
							<span class="title"> <?php esc_html_e( 'Assign events to', 'eventorganiserical' ); ?></span>
							<span class="input-text-wrap">
								<?php
								wp_dropdown_users( array(
									'selected' => $user_id,
									'name'    => 'feed-organiser',
								));
								?>
							</span>
						</label>

						<label>
							<span class="title"> <?php esc_html_e( 'Category', 'eventorganiserical' ); ?></span>
							<span class="input-text-wrap">
								<?php
								wp_dropdown_categories( array(
									'show_option_none' => __( 'Use category specified in feed', 'eventorganiserical' ),
									'orderby'      => 'name',
									'hide_empty'   => 0,
									'hierarchical' => 1,
									'name'         => 'feed-category',
									'id'           => 'feed-category-' . $feed->ID,
									'taxonomy'     => 'event-category',
									'selected'     => $category,
								));
								?>
							</span>
						</label>

						<label>
							<span class="title"> <?php esc_html_e( 'Venue', 'eventorganiserical' ); ?></span>
							<span class="input-text-wrap">
								<?php
								wp_dropdown_categories( array(
									'show_option_none' => __( 'Use venue specified in feed', 'eventorganiserical' ),
									'orderby'      => 'name',
									'hide_empty'   => 0,
									'hierarchical' => 1,
									'name'         => 'feed-venue',
									'id'           => 'feed-venue-' . $feed->ID,
									'taxonomy'     => 'event-venue',
									'selected'     => $venue,
								));
								?>
							</span>
						</label>

						<label>
							<span class="title"> <?php esc_html_e( 'Event Status', 'eventorganiserical' ); ?></span>
							<span class="input-text-wrap">
								<?php
								eventorganiser_select_field(array(
									'name'     => 'feed-status',
									'id'       => 'feed-status',
									'selected' => $status,
									'options'  => array_merge( array( '0' => __( 'Use status specified in feed', 'eventorganiserical' ) ), get_post_statuses() ),
								));
								?>
							</span>
						</label>
					
					</div>		
					
					<input type="hidden" name="id" value="<?php echo esc_attr( $feed->ID ); ?>">
				</fieldset>
	
				<p class="inline-edit-save submit">
					<a href="#inline-edit" title="Cancel" class="cancel button-secondary alignleft">Cancel</a>
					<a id="eo-feed-<?php echo $feed->ID;?>-submit" data-wp-lists="add:feed-list:eo-feed-<?php echo $feed->ID;?>::_ajax_nonce=<?php echo $upd_nonce; ?>" href="#" title="Update Feed" class="save button-primary alignright">
						<?php esc_html_e( 'Update Feed', 'eventorganiserical' ); ?>
					</a>
					<span class="spinner" style="float: right;display: inline-block;visibility: hidden;"></span>
					<span class="error" style="display:none;"></span>		
					<br class="clear">
				</p>
			</td>
		</tr>
	
		<tr class="feed-errors">
			<td colspan="4">
			<?php if ( $error ) : ?>
				<div class="eo-ical-feed-feedback-error eo-ical-feed-feedback"><?php echo esc_html( $error['log'] );?></div>
			<?php elseif ( $warnings ) :

				$warnings = wp_list_pluck( $warnings, 'log' );

				$toggle   = ' <span class="eo-ical-feed-toggle-warnings" aria-expanded=false>
								<a class="eo-ical-feed-show-warnings" href="#">Show warnings</a>
								<a style="display:none" class="eo-ical-feed-hide-warnings" href="#">Hide warning</a>
							</span> ';
				$message = _n(
					'Feed containing %d events successfully imported with %d warning.',
					'Feed containing %d events successfully imported with %d warnings.',
					count( $warnings ),
					'eventorganiserical'
				);
				$summary  = sprintf( $message, $events, count( $warnings ) );

				$feedback = $summary . $toggle . '<div style="display:none" class="eo-ical-feed-warnings">' . implode( '<br>', $warnings ) . '</div>';
				?>
				<div class="eo-ical-feed-feedback-warning eo-ical-feed-feedback">
					<?php echo $feedback; ?>
				</div>
			<?php endif; ?>
			</td>
		</tr>
		</tbody>
		<?php
	}

	function ajax_add_feed() {

		$name      = $_POST['feed-name'];
		$source    = esc_url_raw( $_POST['feed-source'], array( 'http', 'https', 'webcal', 'feed' ) );
		$organiser = isset( $_POST['feed-organiser'] ) ? (int) $_POST['feed-organiser'] : get_current_user_id();
		$status    = isset( $_POST['feed-status'] ) ?  $_POST['feed-status'] : false;
		$category  = isset( $_POST['feed-category'] ) ? (int) $_POST['feed-category'] : 0;
		$venue     = isset( $_POST['feed-venue'] ) ? (int) $_POST['feed-venue'] : 0;

		$old_id = isset( $_POST['id'] ) ? (int) $_POST['id'] : 0;

		if ( ! current_user_can( 'manage_options' ) ) {
			$this->_send_ajax_error( $feed_id, __( "You don't have permission to add feeds", 'eventorganiserical' ) );
		}

		check_ajax_referer( 'add-eo-feed-' . $old_id );

		if ( ! $old_id ) {
			$feed_id = eo_insert_feed( $name, $source, compact( 'organiser', 'status', 'category', 'venue' ) );
		} else {
			$feed_id = eo_update_feed( $old_id, compact( 'name', 'source', 'organiser', 'status', 'category', 'venue' ) );
		}
		$feed = get_post( $feed_id );

		ob_start();
		$this->display_feed_row( $feed );
		$markup = ob_get_contents();
		ob_end_clean();

		//Respond
		$x = new WP_Ajax_Response( array(
			'what'     => 'eo-feed',
			'id'       => $feed_id,
			'old_id'   => $old_id,
			'data'     => $markup,
			'position' => -1,
		));
		$x->send();
		exit();
	}

	function ajax_fetch_feed() {

		$feed_id = isset( $_POST['id'] ) ? (int) $_POST['id'] : 0;

		if ( ! $feed_id ) {
			$this->_send_ajax_error( $feed_id, __( 'No feed specified', 'eventorganiserical' ) );
		}

		if ( ! current_user_can( 'manage_options' ) ) {
			$this->_send_ajax_error( $feed_id, __( "You don't have permission to fetch feeds", 'eventorganiserical' ) );
		}

		check_ajax_referer( 'fetch-eo-feed-' . $feed_id );

		set_time_limit( 600 );

		$response = eo_fetch_feed( $feed_id );

		$feed   = get_post( $feed_id );
		$time   = eo_get_feed_last_fetched( $feed->ID );
		$events = get_post_meta( $feed_id, '_eventorganiser_feed_events_parsed', true );

		if ( $time ) {
			$time_diff = time() - $time;
			if ( $time_diff >= 0 && $time_diff < DAY_IN_SECONDS ) {
				$last_updated = esc_html( sprintf( __( '%s ago' ), human_time_diff( $time ) ) );
			} else {
				$last_updated = date_i18n( get_option( 'date_format' ) . '\<\b\r\/\> ' . get_option( 'time_format' ), $time );
			}
		} else {
			$last_updated = sprintf( '<span aria-hidden="true">—</span><span class="screen-reader-text">%s</span>', esc_html__( 'Not synced', 'eventorganiserical' ) );
		}

		ob_start();
		$this->display_feed_row( $feed );
		$markup = ob_get_contents();
		ob_end_clean();

		$warnings  = get_post_meta( $feed_id, '_eventorganiser_feed_warnings' );
		$error_log = maybe_unserialize( get_post_meta( $feed->ID, '_eventorganiser_feed_log', true ) );
		$error     = false;

		if ( $error_log ) {
			$feedback = $error_log['log'];
			$error    = true;
		} elseif ( ! empty( $warnings ) ) {
			$success = false;
			$warnings = wp_list_pluck( $warnings, 'log' );
			$message = _n(
				'Feed containing %d events successfully imported with %d warning.',
				'Feed containing %d events successfully imported with %d warnings.',
				count( $warnings ),
				'eventorganiserical'
			);
			$summary  = sprintf( $message, $events, count( $warnings ) );
			$toggle = ' <span class="eo-ical-feed-toggle-warnings" aria-expanded=false> 
								<a class="eo-ical-feed-show-warnings" href="#">Show warnings</a>
								<a style="display:none" class="eo-ical-feed-hide-warnings" href="#">Hide warning</a>
							</span> ';
			$feedback = $summary . $toggle . '<div style="display:none" class="eo-ical-feed-warnings">' . implode( '<br>', $warnings ) . '</div>';

		} else {
			$success = true;
			$message = _n(
				'Feed containing %d event successfully imported',
				'Feed containing %d events successfully imported',
				$events,
				'eventorganiserical'
			);
			$feedback = sprintf( $message, $events );
		}

		if ( '' == $events ) {
			$events = sprintf( '<span aria-hidden="true">—</span><span class="screen-reader-text">%s</span>', esc_html__( 'Not synced', 'eventorganiserical' ) );
		} else {
			$events = (int) $events;
		}

		$x = new WP_Ajax_Response( array(
			'what'         => 'eo-feed',
			'id'           => $feed_id,
			'old_id'       => $feed_id,
			'data'         => $markup,
			'supplemental' => compact( 'last_updated', 'timestamp', 'events', 'feedback', 'error', 'success' ),
		));
		$x->send();
	}

	function ajax_delete_feed() {
		$feed_id = isset( $_POST['id'] ) ? (int) $_POST['id'] : 0;

		if ( ! $feed_id ) {
			$this->_send_ajax_error( $feed_id, __( 'No feed specified', 'eventorganiserical' ) );
		}

		if ( ! current_user_can( 'manage_options' ) ) {
			$this->_send_ajax_error( $feed_id, __( "You don't have permission to delete feeds", 'eventorganiserical' ) );
		}

		check_ajax_referer( 'delete-eo-feed-' . $feed_id );

		eo_delete_feed( $feed_id );
		wp_die( 1 );
	}

	function update_feed_settings() {

		if ( ! empty( $_POST['action'] ) && 'eventorganier-update-feed-settings' == $_POST['action'] ) {

			if ( ! current_user_can( 'manage_options' ) ) {
				return;
			}

			check_admin_referer( 'eventorganier-update-feed-settings' );

			$schedule = $_POST['eventorganiser_feed_schedule'];
			update_option( 'eventorganiser_feed_schedule', $schedule );

			wp_clear_scheduled_hook( 'eventorganiser_ical_feed_sync' );

			if ( $schedule ) {
				$schedules = wp_get_schedules();
				$timestamp = time() + $schedules[ $schedule ]['interval'];
				wp_schedule_event( $timestamp, $schedule, 'eventorganiser_ical_feed_sync' );
			}

			wp_redirect( admin_url( 'options-general.php?page=event-settings&tab=imexport&settings-updated=true' ) );
			exit();
		}
	}

	function _send_ajax_error( $feed_id, $feedback ) {
		$x = new WP_Ajax_Response( array(
			'what'         => 'eo-feed',
			'id'           => $feed_id,
			'old_id'       => $feed_id,
			'data'         => false,
			'supplemental' => array(
			'error'    => true,
			'feedback' => $feedback,
			),
		));
		$x->send();
		exit;
	}
}
