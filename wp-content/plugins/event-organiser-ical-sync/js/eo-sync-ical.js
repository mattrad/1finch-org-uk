(function($) {

var eventorganiserFeedManager = {

	init : function(){
		var t = this;
		$('#feed-list').wpList( {
			delBefore: eventorganiserFeedManager.confirmDelete,
			dimBefore: eventorganiserFeedManager.dimBefore,
			dimAfter: eventorganiserFeedManager.dimAfter,
			addBefore: eventorganiserFeedManager.addBefore,
			addAfter: eventorganiserFeedManager.addAfter,
			delAfter: eventorganiserFeedManager.delAfter,
			alt: false
		} );

		$('#feed-list').on( 'click', '.row-actions .edit a', function(e){
			e.preventDefault();
			eventorganiserFeedManager.revert();
			eventorganiserFeedManager.showOptions( $(this) );
		});

		$('#feed-list').on( 'click', 'td.edit-column .cancel', function(e){
			e.preventDefault();
			eventorganiserFeedManager.revert();
		});

		$('#feed-list').on( 'keyup', function(e){
			if ( e.which == 27 )
				return eventorganiserFeedManager.revert();
		});

		$('#feed-list').on( 'focusin', 'tr', function(e){
			var $rowActions = $(this).parents('tbody').find('.row-actions');
			$( '.row-actions' ).not( $rowActions ).removeClass( 'visible' );
			$rowActions.addClass( 'visible' );
		});

		$('#feed-list').on( 'focusout', 'tr', function(e){
			var $rowActions = $(this).parents('tbody').find('.row-actions');
			$rowActions.removeClass( 'visible' );
		});

		$('.eo-advanced-feed-options-toggle').click(function(e){
			e.preventDefault();
			if( $(this).is('.eo-show-advanced-option') ){
				$('#eo-advanced-feed-options-wrap').show();
			}else{
				$('#eo-advanced-feed-options-wrap').hide();
			}

			$(".eo-advanced-feed-options-toggle").not(this).show().focus();
			$(this).hide();
		});

		$('#feed-list').on( 'click', '.eo-ical-feed-toggle-warnings a', function(ev){
			ev.preventDefault();
			eventorganiserFeedManager.toggleWarnings( $(this).parent() );
		});
	},

	showOptions: function( o ){
		var row = o.parents('tbody');
		row.addClass('inline-edit-row');
		row.find('td').hide();
		row.find('td.edit-column').show().find('input:eq(0)').focus();
	},

	revert : function(){
		$('#feed-list td').show();
		$('#feed-list tbody').removeClass('inline-edit-row');
		$('#feed-list td.edit-column').hide();
		return false;
	},

	toggleWarnings: function( $toggle ) {
		var show    = ( 'false' == $toggle.attr( 'aria-expanded' ) );
		$toggle.find('.eo-ical-feed-hide-warnings').toggle( show );
		$toggle.find('.eo-ical-feed-show-warnings').toggle( ! show );
		$toggle
			.attr( 'aria-expanded', show )
			.parent().find('.eo-ical-feed-warnings').toggle( show );
	},

	/**
	 * Triggered before adding / updating a feed
	 * Prevent repeat submissions by checking the lock,
	 * and disable the buttons
	 */
	addBefore: function( s ){
		if( $(s.target).data('eo-lock') ){
			return false;
		}

		$(s.target).data('eo-lock', '1' );
		$(s.target).prop('disabled', true).addClass('disabled'); //add class as it may be a link
		$(s.target).parent().find('.spinner').css( 'visibility', 'visible' );

		return s;
	},

	/**
	 * Triggered after adding / updating a feed
	 * Remove lock and reset buttons
	 */
	addAfter : function( r, s ){
		$(s.target).removeData('eo-lock');
		$(s.target).prop('disabled', false).removeClass('disabled');
		$(s.target).parent().find('.spinner').css( 'visibility', 'hidden' );

		var res = wpAjax.parseAjaxResponse(r, s.response, s.element), message;

		if ( !res || res.errors ) {
			if( !res ){
				message = 'Unknown error';
			}else{
				message = res.responses[0].errors[0].message;
			}
			$('#'+s.element+' .feed-errors td').append( '<div class="eo-ical-feed-feedback-error feed-alert"/>') .text( message );
		}else{
			$('#eo-feed-no-feeds').hide();
		}

		var $row = $( '#'+this.what + '-' + res.responses[0].id );
		$row.find('.eo-ical-feed-name').focus();
	},

	getId : function( o ) {
		var id = $(o).closest('tbody').attr('id'),
		parts = id.split('-');
		return parts[parts.length - 1];
	},

	confirmDelete: function( s ){
		if ( confirm( "You are about to permanently delete this feed.\n 'Cancel' to stop, 'OK' to delete." ) ) {
			$row = $('#'+this.what+'-'+s.data.id);
			$goto = $row.next('.eo-ical-feed:visible');

			if ( ! $goto.length ) {
				$goto = $row.prev('.eo-ical-feed:visible');
			}
			if ( ! $goto.length ) {
				$goto = $('#add-eo-feed input:eq(0)');
				$('#eo-feed-no-feeds').show();
			} else {
				$goto = $goto.find('.eo-ical-feed-name');
			}

			$goto.focus();
			return s;
		}
		return false;
	},

	delAfter: function( r, s ){
		var res = wpAjax.parseAjaxResponse(r, s.response, s.element), message;
		if ( !res || res.errors ) {
			if( !res ){
				message = 'Unknown error';
			}else{
				message = res.responses[0].errors[0].message;
			}
			$('#'+s.element+' .feed-errors td').html( '<div class="eo-ical-feed-feedback-error feed-alert"/>').find('.eo-ical-feed-feedback-error').text( message );
		}else{
			$row = $('#'+this.what+'-'+s.data.id);
			$row.remove();
		}
	},

	dimBefore: function( s ){

		if( 'fetch-eo-feed' == s.data.action ){
			if( $(s.target).data('eo-lock') ){
				return false;
			}else{
				$(s.target).data('eo-lock', '1' );
			}

			$tr = $(s.target).parents('tr');
			$tr.find('.last-updated .spinner').css( 'visibility', 'visible' ).addClass('is-active');
			$tr.find('.eo-ical-feed-last-updated-date').hide();
			$tr.parents('tbody').find('.eo-ical-feed-feedback').hide();
			$tr.find('.feed-events').text('');
			$(s.target).css({ 'color': '#aaa', cursor: 'default' });

			$('#'+this.what+'-'+s.data.id).attr('class','eo-ical-feed-fetching');
		}
		return s;
	},

	dimAfter: function( r, s ){

		if( 'fetch-eo-feed' == s.data.action ){

			var $row = $('#'+this.what+'-'+s.data.id);
			var res  = wpAjax.parseAjaxResponse(r, s.response, s.element), message;

			$(s.target).css({ 'color': '', cursor: 'pointer' });
			$(s.target).removeData('eo-lock');

			var feedClass, error;

			if ( !res || res.errors ) {
				error = true;
				warnings = false;
				if( !res ){
					feedback = 'An unknown error has occurred';
				}else{
					feedback = res.responses[0].errors[0].message;
				}

			} else {
				feedback = res.responses[0].supplemental.feedback;
				error    = res.responses[0].supplemental.error;
				success  = res.responses[0].supplemental.success;
			}

			if ( error ) {
				$('#'+s.element+' .feed-errors td').html( '<div class="eo-ical-feed-feedback-error eo-ical-feed-feedback">' +  feedback + '</div>' );
				feedClass = 'eo-ical-feed-with-errors';
			} else if ( ! success ) {
				$('#'+s.element+' .feed-errors td').html( '<div class="eo-ical-feed-feedback-warning eo-ical-feed-feedback">' + feedback + '</div>' );
				feedClass = 'eo-ical-feed-with-warnings';
			} else {
				feedClass = 'eo-ical-feed-success';
			}

			jQuery.each( res.responses, function() {
				$row.find( 'td.feed-events').html( this.supplemental.events );
				$row.find( 'td.last-updated .eo-ical-feed-last-updated-date').html( this.supplemental.last_updated );
			} );

			$tr = $(s.target).parents('tr');
			$tr.find('.last-updated .spinner').css( 'visibility', 'hidden' ).removeClass('is-active');
			$tr.find('.eo-ical-feed-last-updated-date').show();
			$row.attr('class','eo-ical-feed ' + feedClass);
		}
		return s;
	}
};


$(document).ready(function(){
	eventorganiserFeedManager.init();
});

})(jQuery);
