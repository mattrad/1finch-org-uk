<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
  * Manage iCal feeds
  */
class EO_Event_Ical_CLI_Command extends \WP_CLI\CommandWithDBObject {

	protected $obj_id_key = 'ID';

	protected $obj_fields = array(
		'ID',
		'name',
		'source',
		'events',
		'last_updated',
	);

	/**
	 * Lists iCal feeds.
	 *
	 * ## OPTIONS
	 *
	 * [--field=<field>]
	 * : Prints the value of a single field for each feed.
	 *
	 * [--fields=<fields>]
	 * : Limit the output to specific object fields.
	 *
	 * [--format=<format>]
	 * : Accepted values: table, csv, json, count, ids. Default: table
	 *
	 * ## AVAILABLE FIELDS
	 *
	 * * ID
	 * * name
	 * * last_updated
	 * * events
	 * * source
	 *
	 * ## EXAMPLES
	 *
	 *     wp eo ical-feed list --field=name
	 *
	 *     wp eo ical-feed list --format=json
	 *
	 *     wp eo ical-feed list --format=ids
	 *
	 * @subcommand list
	 */
	public function list_( $args, $assoc_args ) {

		$formatter = $this->get_formatter( $assoc_args );

		if ( 'ids' == $formatter->format ) {
			$feeds = eo_get_feeds( array( 'fields' => 'ids' ) );
			echo implode( ' ', $feeds );
		} else {
			$feeds = eo_get_feeds();
			if ( $feeds ) {
				$feeds = array_map( function( $feed ) {
					$feed->name         = $feed->post_title;
					$feed->last_updated = $feed->post_modified;
					$feed->events       = (int) get_post_meta( $feed->ID, '_eventorganiser_feed_events_parsed', true );
					$feed->source       = urldecode( get_post_meta( $feed->ID, '_eventorganiser_feed_source', true ) );
					return $feed;
				}, $feeds );
			}
			$formatter->display_items( $feeds );
		}

	}

	/**
	 * Fetches (imports) a specific iCal feed or all feeds
	 *
	 * ## OPTIONS
	 *
	 * [<id>...]
	 * : One or more IDs of feeds to delete.
	 *
	 * [--all]
	 * : Fetches all events
	 *
	 * ## EXAMPLES
	 *
	 *     wp eo ical-feed fetch 123 456
	 *
	 *     wp eo ical-feed fetch --all
	 *
	 */
	public function fetch( $args, $assoc_args ) {

		$status = 0;

		if ( ! empty( $assoc_args['all'] ) ) {
			$args = eo_get_feeds( array( 'fields' => 'ids', 'numberposts' => -1 ) );
		} elseif ( empty( $args ) ) {
			\WP_CLI::error( 'Please specify at least one feed ID' );
		}

		foreach ( $args as $feed_id ) {

			$feed_id = (int) $feed_id;

			if ( ! eo_is_ical_feed( $feed_id ) ) {
				return array( 'error', sprintf( '%s is not an iCal feed ID', $feed_id ) );
			}

			$name = get_the_title( $feed_id );

			\WP_CLI::log( "Fetching feed '{$name}' ($feed_id)..." );

			$response = eo_fetch_feed( $feed_id );

			if ( ! $response ) {
				$error = maybe_unserialize( get_post_meta( $feed_id, '_eventorganiser_feed_log', true ) );
				\WP_CLI::warning( "Error fetching feed '{$name}' ($feed_id)..." );
				\WP_CLI::warning( $error['log'] );
				$status = 1;

			} else {
				\WP_CLI::success( "Fetched feed '{$name}' ($feed_id)..." );
			}

			if ( count( $args ) ) {
				\WP_CLI::log( '' );
			}
		}

		exit( $status );
	}

	/**
	 * Creates an iCal feed.
	 *
	 * ## OPTIONS
	 *
	 * --name=<value>
	 * : The name of the field
	 *
	 * --source=<value>
	 * : The URL of the iCal feed
	 *
	 * [--<field>=<value>]
	 * : Associative args for the new post. See eo_insert_feed().
	 *
	 * [--porcelain]
	 * : Output just the new feed id.
	 *
	 * ## AVAILABLE FIELDS
	 *
	 * * status - The status to assign the events in this feed to
	 * * organiser - The user ID to assign ownership of events in this feed to
	 * * category - The event category ID to assign the events in this feed to
	 *
	 * ## EXAMPLES
	 *
	 *     wp eo ical-feed create --name='My feed' --source=http://my-source.example/ical
	 *
	 *     wp eo ical-feed create --name='My feed' --source=http://my-source.example/ical --status=draft
	 */
	public function create( $args, $assoc_args ) {

		if ( empty( $assoc_args['name'] ) ) {
			\WP_CLI::error( 'Please specify a feed name' );
		} elseif ( empty( $assoc_args['source'] ) ) {
			\WP_CLI::error( 'Please specify a feed source' );
		}

		parent::_create( $args, $assoc_args, function ( $params ) {
			$args = array_intersect_key( $params, array(
				'organiser' => null,
				'category'  => 0,
				'venue'     => 0,
				'status'    => 0,
			) );
			$feed_id = eo_insert_feed( $params['name'], $params['source'], $args );
			return $feed_id;
		} );

	}

	/**
	 * Update one or more feeds.
	 *
	 * ## OPTIONS
	 *
	 * <id>...
	 * : One or more IDs of feeds to update.
	 *
	 * --<field>=<value>
	 * : One or more fields to update. See eo_update_feed().
	 *
	 * ## AVAILABLE FIELDS
	 *
	 * * name - A name for the field to help identify it
	 * * source - The feed source. A url or path to the iCal file
	 * * status - The status to assign the events in this feed to
	 * * organiser - The user ID to assign ownership of events in this feed to
	 * * category - The event category ID to assign the events in this feed to
	 *
	 * ## EXAMPLES
	 *
	 *     wp eo ical-feed update 123 --name=something --status=draft
	 */
	public function update( $args, $assoc_args ) {
		foreach ( $args as $key => $arg ) {
			if ( is_numeric( $arg ) ) {
				continue;
			}
			unset( $args[ $key ] );
			break;
		}

		parent::_update( $args, $assoc_args, function ( $params ) {
			$feed_id = $params[ $this->obj_id_key ];
			unset( $params[ $this->obj_id_key ] );
			return eo_update_feed( $feed_id, $params );
		} );
	}

	/**
	 * Delete an iCal feed by ID (and optionally its events also).
	 *
	 * ## OPTIONS
	 *
	 * <id>...
	 * : One or more IDs of feeds to delete.
	 *
	 * [--delete-events]
	 * : Delete/trash events imported by this feed
	 *
	 * [--force]
	 * : Delete rather than trash events (requires --delete-events)
	 *
	 * ## EXAMPLES
	 *
	 *     wp eo ical-feed delete 123 --delete-events --force
	 *
	 *     wp eo ical-feed delete $(wp eo ical-feed list --format=ids)
	 *
	 */
	public function delete( $args, $assoc_args ) {

		if ( empty( $args ) || ! intval( $args[0] ) ) {
			\WP_CLI::error( 'Please specify an iCal feed ID' );
		} elseif ( count( $args ) > 1 ) {
			\WP_CLI::error( 'Please specify only one iCal feed ID' );
		}

		$feed_id = $args[0];

		parent::_delete( $args, $assoc_args, function ( $feed_id, $assoc_args ) {
			$status = get_post_status( $feed_id );
			if ( ! eo_is_ical_feed( $feed_id ) ) {
				return array( 'error', sprintf( '%s is not an iCal feed ID', $feed_id ) );
			}

			if ( ! empty( $assoc_args['delete-events'] ) ) {
				$this->_delete_events(
					array( $feed_id ),
					array( 'force' => ! empty( $assoc_args['force'] ) )
				);
			}

			$r = wp_delete_post( $feed_id, true );
			if ( $r ) {
				return array( 'success', "Deleted feed $feed_id." );
			} else {
				return array( 'error', "Failed deleting feed $feed_id." );
			}
		} );

	}

	/**
	 * Delete the events of an iCal feed.
	 *
	 * ## OPTIONS
	 *
	 * <feed-id>
	 * : Feed ID to trash/delete the events of
	 *
	 * [--force]
	 * : Delete rather than trash events
	 *
	 * ## EXAMPLES
	 *
	 *     wp eo ical-feed delete-events 123
	 *
	 * @subcommand delete-events
	 */
	public function delete_events( $args, $assoc_args ) {
		$status = $this->_delete_events( $args, $assoc_args );
		exit( $status );
	}

	protected function _delete_events( $args, $assoc_args ) {

		$status = 0;

		$defaults = array(
			'force' => false,
		);
		$assoc_args = array_merge( $defaults, $assoc_args );

		if ( empty( $args ) || ! intval( $args[0] ) ) {
			\WP_CLI::error( 'Please specify an iCal feed ID' );
		} elseif ( count( $args ) > 1 ) {
			\WP_CLI::error( 'Please specify only one iCal feed ID' );
		}

		$feed_id = $args[0];
		
		if ( ! eo_is_ical_feed( $feed_id ) ) {
			\WP_CLI::error( sprintf( '%s is not an iCal feed ID', $feed_id ) );
		}

		$delete_events = eo_get_events( array(
			'numberposts'            => -1,
			'post_status'            => 'any',
			'fields'                 => 'ids',
			'no_found_rows'          => true,
			'showpastevents'         => true,
			'group_events_by'        => 'series',
			'update_post_term_cache' => false,
			'update_meta_term_cache' => false,
			'meta_query' => array(
			array(
					'key' => '_eventorganiser_feed',
					'value' => $feed_id,
				),
			),
		));

		if ( $delete_events ) {
			foreach ( $delete_events as $event_id ) {
				$status   = get_post_status( $event_id );

				if ( $assoc_args['force'] ) {
					$response = wp_delete_post( $event_id, $assoc_args['force'] );
				} else {
					$response = wp_trash_post( $event_id );
				}

				if ( $response ) {
					$action = $assoc_args['force'] || 'trash' === $status ? 'Deleted' : 'Trashed';
					$response = array( 'success', "$action event $event_id." );
				} else {
					$response = array( 'error', "Failed deleting post $event_id." );
				}

				$status = $this->success_or_failure( $response );
			}
		}

		return $status;
	}

	/**
	 * Validate an iCal feed.
	 *
	 * ## OPTIONS
	 *
	 * <source>
	 * : URL of iCal feed to validate or integer of the feed ID to validate
	 *
	 * [--error-levels]
	 * : Comma delimited string of levels to show. Defaultsto errors,warnings
	 *
	 *
	 * [--context]
	 * : How many lines either side of the error/warning to show
	 *
	 * ## EXAMPLES
	 *
	 *     wp eo ical-feed validate http://www.url-to/ical.ics
	 *
	 *     wp eo ical-feed validate 123
	 *
	 */
	public function validate( $args, $assoc_args ) {

		$source = isset( $args[0] ) ? $args[0] : false;

		$assoc_args = array_merge( array(
			'error-levels'  => array( 'errors', 'warnings' ),
			'context'       => 2,
		), $assoc_args );

		if ( ! is_array( $assoc_args['error-levels'] ) ) {
			$assoc_args['error-levels'] = explode( ',', $assoc_args['error-levels'] );
			$assoc_args['error-levels'] = array_map( 'trim', $assoc_args['error-levels'] );
		}

		$source = ctype_digit( $source ) ? eo_get_feed_source( $source ) : $source;

		if ( ! $source ) {
			WP_CLI::error( 'You must specify a source' );
		}

		$ical = new EO_ICAL_Parser();
		$ical->remote_timeout = 600;
		$response = $ical->parse( $source );

		$abort = array( 'invalid-ical-source', 'unable-to-read', 'unable-to-fetch' );
		if ( is_wp_error( $response ) && in_array( $response->get_error_code(), $abort ) ) {
			$this->error( $response->get_error_message() );
			return;
		}

		$lines   = (string) count( $ical->ical_array );
		$padding = strlen( $lines );

		$context = $assoc_args['context'];
		$levels  = $assoc_args['error-levels'];

		$out = '';

		foreach ( $ical->ical_array  as $line => $content ) {

			$line = $line + 1;

			$report = array(
				'level'    => null,
				'feedback' => null,
			);

			foreach ( $levels as $level ) {
				if ( $ical->$level ) {
					foreach ( $ical->$level as $error ) {

						//Data from error
						$data = $error->get_error_data();

						//Ensure the error's line reference is an array
						$err_lines = is_array( $data['line'] ) ? $data['line'] : array( 'start' => $data['line'], 'end' => $data['line'] );

						//If this line is an error, flag it as such
						if ( $line >= $err_lines['start'] && $line <= $err_lines['end'] ) {
							$report['level'] = $level;

							//After the last line of an error, include the error messages
							if ( $line == $err_lines['end'] ) {
								$report['feedback'] = $error->get_error_message();
							}
							break 2;

							//Otherwise if this line is within a context an error, flag it as such
						} else if ( $line >= $err_lines['start'] - $context && $line <= $err_lines['end'] + $context ) {
							$report['level'] = 'context';
						}
					}
				}
			}

			switch ( $report['level'] ) {
				case 'errors':
					$colour = '%r';
					break;
				case 'warnings':
					$colour = '%y';
					break;
				case 'context':
					$colour = '%w';
					break;

			}

			if ( ! $report['level'] ) {
				continue;
			}

			//Print line
			$output = wordwrap( str_pad( $line.'. ', $padding + 2, ' ' ) . $content, 90 );
			$output = str_pad( $line.'. ', $padding + 2, ' ' ) . $content;
			$out .= wordwrap( WP_CLI::colorize( "$colour$output%n", true ) . "\n", 90 );

			//Print any error/warning messages
			if ( $report['feedback'] ) {
				$feedback = str_pad( '', $padding + 2, ' ' ) . $report['feedback'];
				$colour = strtoupper( $colour );
				$out .= wordwrap( WP_CLI::colorize( "$colour$feedback%n", true ), 90 ) . "\n";
			}
		}

		//SUMMARY
		$colour = '%2';
		if ( count( $ical->errors ) ) {
			$colour = '%1';
		} else if ( count( $ical->warnings ) ) {
			$colour = '%3';
		}

		$summary = $this->_summary(
			sprintf( '%d events %d errors %d warnings', count( $ical->events ), count( $ical->errors ), count( $ical->warnings ) ),
			$colour
		);

		$this->pass_through_pager( $summary . $out );

	}

	protected function error( $message ) {
		$this->_summary( $message, '%1' );
	}

	protected function warning( $message ) {
		$this->_summary( $message, '%3' );
	}

	protected function _summary( $message, $colour ) {
		$screenwidth = exec( 'tput cols' );
		$message = str_pad( $message, $screenwidth, ' ', STR_PAD_BOTH );
		return WP_CLI::colorize( "$colour$message%n", true ) . "\n";
	}

	private static function pass_through_pager( $out ) {
		if ( false === ( $pager = getenv( 'PAGER' ) ) ) {
			$pager = \WP_CLI\Utils\is_windows() ? 'more' : 'less -r';
		}
		// convert string to file handle
		$fd = fopen( 'php://temp', 'r+' );
		fputs( $fd, $out );
		rewind( $fd );
		$descriptorspec = array(
			0 => $fd,
			1 => STDOUT,
			2 => STDERR,
		);
		return proc_close( proc_open( $pager, $descriptorspec, $pipes ) );
	}
}

WP_CLI::add_command( 'eo ical-feed', 'EO_Event_Ical_CLI_Command' );
