<?php
/**
 * 
 * Simple History Must-Use Functions
 * 
 */

// This is to check logins not from a certain IP 
add_filter( 'simple_history/log_insert_data', function ( $data ) {
    $my_ip = '1.2.3.4';

    if ( ! empty( $_SERVER['REMOTE_ADDR'] ) && $my_ip !== $_SERVER['REMOTE_ADDR'] ) {
        $data['level'] = 'critical';
    }

    return $data;
} );

// Skip loading of loggers, because overload
add_filter('simple_history/logger/load_logger', function( $load_logger, $oneLoggerFile ) {
    
    // Don't load loggers for comments or menus, i.e. don't log changes to comments or to menus
    if ( in_array( $oneLoggerFile, array( 'SimpleCommentsLogger', 'SimpleMenuLogger' ) ) ) {
        $load_logger = false;
    }

    return $load_logger;

}, 10, 2);
    
 // Set Slack webhook url
add_filter("simple_history/developer_loggers/slackit/settings", function($settings) {
    
    $settings["webhook_url"] = "https://hooks.slack.com/services/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";

    return $settings;
    
}, 10, 1);

// Stop users clearing the log
add_filter( 'simple_history/user_can_clear_log', function ( $user_can_clear_log ) {
    
    $user_can_clear_log = false;
    
    return $user_can_clear_log;

});

// Clear items that are older than a 1 day. It's going to Slack/elsewhere anyway.
add_filter( 'simple_history/db_purge_days_interval', function( $days ) {
    
        $days = 1;
    
        return $days;
    
    } );

// Remove ability to view RSS feed of events
add_filter( 'simple_history/dropin/load_dropin_SimpleHistoryRSSDropin', '__return_false' );